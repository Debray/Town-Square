document.addEventListener("DOMContentLoaded", function(event) { 


	//////////////////////////////////////////////////////////////////////////////////
	//		Init
	//////////////////////////////////////////////////////////////////////////////////

	// init renderer
	var renderer	= new THREE.WebGLRenderer({
		antialias: true,
		alpha: true
	});
	renderer.setClearColor(new THREE.Color('lightgrey'), 0)
	renderer.setSize( 640, 480 );
	renderer.domElement.style.position = 'absolute'
	renderer.domElement.style.top = '0px'
	renderer.domElement.style.left = '0px'
	document.body.appendChild( renderer.domElement );


	//init DOM variables
	var cons = document.getElementById("developpersDisplay"); 
	var pointer = document.getElementById("pointer"); 
	var findMarkerMask = document.getElementById("findMarkerMask");
	var splashScreen = document.getElementById("splashScreen");
	var splashScreenMask = document.getElementById("splashScreenMask");
	var counter = document.getElementById("counter");
	var markerLogo = document.getElementById("markerLogo");
	var titleBig = document.getElementById("titleBig");
	var markerExplanation = document.getElementById("markerExplanation");
	var linkMarker = document.getElementById("linkMarker");
	var formURL = document.getElementById("formURL");

	var onRenderFcts= [];

	// init scene and camera
	var scene = new THREE.Scene();


	//////////////////////////////////////////////////////////////////////////////////
	//		Initialize a basic camera
	//////////////////////////////////////////////////////////////////////////////////

	// Create a camera
	var camera = new THREE.Camera();
	scene.add(camera);

	//create lights
	var ambLight = new THREE.HemisphereLight(0xffffff,0xffffff,0.5);
	scene.add(ambLight);

	var directionalLight = new THREE.DirectionalLight( 0xffffff, 0.4 );
	directionalLight.position.set(15,4,0);
	directionalLight.castShadow = true;
	directionalLight.shadowDarkness = 0.5;
	directionalLight.shadow.mapSoft = true;
	directionalLight.shadow.mapWidth = 1024;
	directionalLight.shadow.mapHeight = 1024;

	scene.add( directionalLight );

	var directionalLight2 = new THREE.DirectionalLight( 0xffffff, 0.3 );
	directionalLight2.position.set(0,10,5);
	directionalLight2.castShadow = true;
	directionalLight2.shadowDarkness = 0.5;
	directionalLight2.shadow.mapSoft = true;
	directionalLight2.shadow.mapWidth = 1024;
	directionalLight2.shadow.mapHeight = 1024;

	scene.add( directionalLight2 );

	//create positionnal audio listenner for sound spatialization
	var listener = new THREE.AudioListener();
	camera.add( listener );
	var soundMusic = new THREE.PositionalAudio( listener );
	var soundFly = new THREE.PositionalAudio( listener );
	var flyToCome = new THREE.PositionalAudio( listener );
	var onlyBass = new THREE.PositionalAudio( listener );
	var x = 0;

	var audioLoader = new THREE.AudioLoader();

	var loader = new THREE.JSONLoader();
	var textureLoader = new THREE.TextureLoader();



	////////////////////////////////////////////////////////////////////////////////
	//          handle arToolkitSource
	////////////////////////////////////////////////////////////////////////////////

	var arToolkitSource = new THREEx.ArToolkitSource({
		// to read from the webcam 
		sourceType : 'webcam',
			
	})

	function askForWebcam (){

		arToolkitSource.init(function onReady(){
			onResize();
			titleBig.setAttribute("style", "visibility:hidden");
			splashScreen.setAttribute("style", "visibility:hidden");
			splashScreenMask.setAttribute("style", "visibility:hidden");
			findMarkerMask.setAttribute("style", "visibility:visible;position:absolute;top:50%;left:50%;height: 300px;display: table;z-index: 3;width: 300px;border:1.5px solid white;border-radius:10px;margin-left:-150px;margin-top:-150px;");
			markerExplanation.setAttribute("style", "visibility:visible;font-size:15px;color:white;position:absolute;top:50%;left:50%;height: 300px;display: table;z-index: 3;width: 270px;margin-left:-130px;margin-top:-130px;");

		})

		// handle resize
		window.addEventListener('resize', function(){
			onResize()
		})
		function onResize(){
			arToolkitSource.onResize()	
			arToolkitSource.copySizeTo(renderer.domElement)	
			if( arToolkitContext.arController !== null ){
				arToolkitSource.copySizeTo(arToolkitContext.arController.canvas)	
			}	
		}
		////////////////////////////////////////////////////////////////////////////////
		//          initialize arToolkitContext
		////////////////////////////////////////////////////////////////////////////////


		// create atToolkitContext
		var arToolkitContext = new THREEx.ArToolkitContext({
			cameraParametersUrl: 'Data/camera_para.dat',
			//detectionMode: 'mono',
			detectionMode:'mono_and_matrix',
			matriCodeType :'3x3'
		})
		// initialize it
		arToolkitContext.init(function onCompleted(){
			// copy projection matrix to camera
			camera.projectionMatrix.copy( arToolkitContext.getProjectionMatrix() );
		})

		// update artoolkit on every frame
		onRenderFcts.push(function(){
			if( arToolkitSource.ready === false )	return

			arToolkitContext.update( arToolkitSource.domElement )
			
			// update scene.visible if the marker is seen
			scene.visible = camera.visible
		})
			
		////////////////////////////////////////////////////////////////////////////////
		//          Create a ArMarkerControls
		////////////////////////////////////////////////////////////////////////////////

		// init controls for camera
		var markerControls = new THREEx.ArMarkerControls(arToolkitContext, camera, {
			//type : 'pattern',
			type : 'barcode',
			barcodeValue : 20,
			//patternUrl : 'Data/patt.hiro',
			// as we controls the camera, set changeMatrixMode: 'cameraTransformMatrix'
			changeMatrixMode: 'cameraTransformMatrix'
		})
		// as we do changeMatrixMode: 'cameraTransformMatrix', start with invisible scene
		scene.visible = false

	}


	//A game object contains is the class of objects you can import from a JSON file
	function GameObject(iName, iModelName, iPositionArray, iRotationArray, iScaleArray){
		this.name = iName;
		this.modelName = iModelName;
		this.positionArray = iPositionArray;
		this.rotationArray = iRotationArray;
		this.scaleArray = iScaleArray;
		this.id="";
	}

	GameObject.prototype.display = function (){
		var obj = scene.getObjectByName( this.name );
		if (obj)
			obj.visible = true;
	}

	GameObject.prototype.hide = function (){
		var obj = scene.getObjectByName( this.name );
		if (obj)
			obj.visible = false;
	}


	//the Ribbon is not used in this version of the game but can be used to create stunning effect of moving scenary
	function Ribbon( iGameObjectArray, iDirection, iSpeed, iVisibleWidth ){
		this.gameObjectArray = iGameObjectArray;
		this.direction = iDirection;
		this.speed = (typeof iSpeed !== 'undefined') ?  iSpeed : 0.05;
		this.visibleWidth = (typeof iVisibleWidth !== 'undefined') ?  iVisibleWidth : 3; 
	};

	Ribbon.prototype.update = function (){
		for (i = 0; i < this.gameObjectArray.length; i++){
			var obj = scene.getObjectByName( this.gameObjectArray[i].name );
			if (typeof obj !== 'undefined')	{
				if (obj.position.x> this.visibleWidth/2){
					obj.position.x= -this.visibleWidth/2;
					obj.material.opacity = 0;
				}

				if (obj.position.x>-this.visibleWidth/2 && obj.position.x<this.visibleWidth*(1/3)){
					if (obj.material.opacity<=this.visibleWidth*(1/3))
					obj.material.opacity += 0.1;
				}

				if (obj.position.x>this.visibleWidth*(1/3)){
					obj.material.opacity -= 0.1;
				}
				obj.position.x += this.speed;
			}			
		}
	}


	/*
		The Scene is a group of fixed objects used for scenery, it contains time sequences
	*/
	function Scene( iSceneObjects, iSequences, iRibbon ){
		this.sceneObjects = iSceneObjects;
		this.sequences = iSequences;
		this.ribbon = (typeof iRibbon !== 'undefined') ?  iRibbon : "none";
		this.toLoad = 0;
		this.texturesToLoad = 0;
		this.sequencesToLoad = -1;
		this.toPlay = false;
	};

	Scene.prototype.init = function (){
		this.sequencesToLoad = this.sequences.length+4;
		this.toLoad = this.sceneObjects.length;
		this.texturesToLoad = this.sceneObjects.length;
		this.loadTextures();
		this.loadSequences();
	}

	Scene.prototype.display = function (){
		for ( var i = 0; i < this.sceneObjects.length; i++ ){
			this.sceneObjects[i].display();
		}
		//scene.getObjectByName("arrowAnim").add(sound);
	}

	Scene.prototype.loadTextures = function (){
		var scope = this;

		scope.loadModels ();
	}
	/*

	Scene.prototype.loadModels = function (){
		var scope = this;

		for ( var j = 0; j < this.sceneObjects.length; j++ ) {

			
			var gameObject = this.sceneObjects[j];

			loader.load('./Models/'+gameObject.modelName+'.json', function(geometry, materials) {
				var material = new THREE.MeshPhongMaterial( { 
					color: 0x8cc5eb , 
					specular: 0x808080 , 
					shininess: 10,
					transparent:true,
	    			clipShadows: true 
				});

	    		//var obj3D = new THREE.Mesh(geometry, new THREE.MeshFaceMaterial(materials));

	    		var gameObject = scope.sceneObjects[(scope.sceneObjects.length-scope.toLoad)];
	    		var obj3D = new THREE.Mesh(geometry, material);
	    		obj3D.name = gameObject.name;
	    		obj3D.visible = false;
	    		scene.add(obj3D);
	    		//obj3D.add( soundMusic );
	    		gameObject.id = obj3D.id;

				obj3D.scale.set( gameObject.scaleArray[0], gameObject.scaleArray[1], gameObject.scaleArray[2] );
	    		obj3D.rotateX( gameObject.rotationArray[0] );
	    		obj3D.rotateY( gameObject.rotationArray[1] );
	    		obj3D.rotateZ( gameObject.rotationArray[2] );
	    		obj3D.position.set( gameObject.positionArray[0], gameObject.positionArray[1], gameObject.positionArray[2] );
	    		scope.toLoad--;
	    		if ( scope.toLoad === 0 ){
	    			scope.display();
	    		};
			});

		}
	}*/


	Scene.prototype.loadModels = function (){
		var scope = this;

		for ( var j = 0; j < this.sceneObjects.length; j++ ) {

			
			var gameObject = this.sceneObjects[j];

			loader.load(j, './Models/'+gameObject.modelName+'.json', function(geometry, materials, n) {
				materials.forEach(function (material) {
				  material.skinning = true;
				});

	    		//var obj3D = new THREE.Mesh(geometry, new THREE.MeshFaceMaterial(materials));

	    		//var gameObject = scope.sceneObjects[(scope.sceneObjects.length-scope.toLoad)];
	    		var gameObject = scope.sceneObjects[n];
	    		var obj3D = new THREE.SkinnedMesh( geometry, new THREE.MeshFaceMaterial(materials));
	    		obj3D.name = gameObject.name;
	    		obj3D.visible = false;
	    		scene.add(obj3D);
	    		//obj3D.add( soundMusic );
	    		gameObject.id = obj3D.id;

				obj3D.scale.set( gameObject.scaleArray[0], gameObject.scaleArray[1], gameObject.scaleArray[2] );
	    		obj3D.rotateX( gameObject.rotationArray[0] );
	    		obj3D.rotateY( gameObject.rotationArray[1] );
	    		obj3D.rotateZ( gameObject.rotationArray[2] );
	    		obj3D.position.set( gameObject.positionArray[0], gameObject.positionArray[1], gameObject.positionArray[2] );
	    		scope.toLoad--;
	    		if ( scope.toLoad === 0 ){
	    			scope.display();
	    		};
			});

		}
	}

	//TODO this part of the code is really ugly and needs to be fixed by factorizing the callbacks for music file loading 
	Scene.prototype.loadSequences = function (){
		var scope = this;
		var ord = 0;

		for ( var m = 0; m < this.sequences.length; m++ ) {
			scope.sequences[m].load(function(iSequenceName){
				scope.sequencesToLoad--;
				ord = (scope.sequencesToLoad/scope.sequences.length+4)*75;

				splashScreenMask.setAttribute("style","visibility:visible;position:absolute;height: 100%;display: table;z-index: 10;width: 100%;background-image: url('./Data/loaded.png');background-size:cover;background-position:center;clip-path: polygon(0 "+ord+"%, 100% "+ord+"%, 100% 100%, 0% 100%);");

	    		if ( scope.sequencesToLoad === 0 ){
	    			console.log("Everything loaded !");
	    			splashScreen.setAttribute("style", "opacity:0.5;visibility:visible;position:absolute;height: 100%;display: table;z-index: -1;width: 100%;background-image: url('./Data/splashScreeeeen.jpg');background-size:cover;background-position:center;");
  					splashScreenMask.setAttribute("style","visibility:visible;position:absolute;height: 100%;display: table;z-index: 10;width: 100%;background-image: url('./Data/loaded.png');background-size:cover;background-position:center;");

	    			askForWebcam();
	    		};
			});
		}
		audioLoader.load( './Sounds/music.mp3', function( buffer ) {
			soundMusic.setBuffer( buffer );
			soundMusic.setRefDistance( 1 );
			scope.sequencesToLoad--;
			ord = (scope.sequencesToLoad/scope.sequences.length+3)*75;

			splashScreenMask.setAttribute("style","visibility:visible;position:absolute;height: 100%;display: table;z-index: 10;width: 100%;background-image: url('./Data/loaded.png');background-size:cover;background-position:center;clip-path: polygon(0 "+ord+"%, 100% "+ord+"%, 100% 100%, 0% 100%);");

    		if ( scope.sequencesToLoad === 0 ){
    			console.log("Everything loaded !");
    			splashScreen.setAttribute("style", "opacity:0.5;visibility:visible;position:absolute;height: 100%;display: table;z-index: -1;width: 100%;background-image: url('./Data/splashScreeeeen.jpg');background-size:cover;background-position:center;");
  				splashScreenMask.setAttribute("style","visibility:visible;position:absolute;height: 100%;display: table;z-index: 10;width: 100%;background-image: url('./Data/loaded.png');background-size:cover;background-position:center;");

    			askForWebcam();
    		};
		});

		audioLoader.load( './Sounds/fly.mp3', function( buffer ) {
			soundFly.setBuffer( buffer );
			soundFly.setRefDistance( 1 );

			scope.sequencesToLoad--;
			ord = (scope.sequencesToLoad/scope.sequences.length+3)*75;

			splashScreenMask.setAttribute("style","visibility:visible;position:absolute;height: 100%;display: table;z-index: 10;width: 100%;background-image: url('./Data/loaded.png');background-size:cover;background-position:center;clip-path: polygon(0 "+ord+"%, 100% "+ord+"%, 100% 100%, 0% 100%);");

    		if ( scope.sequencesToLoad === 0 ){
    			console.log("Everything loaded !");
  				splashScreenMask.setAttribute("style","visibility:visible;position:absolute;height: 100%;display: table;z-index: 10;width: 100%;background-image: url('./Data/loaded.png');background-size:cover;background-position:center;");
    			
    			splashScreen.setAttribute("style", "opacity:0.5;visibility:visible;position:absolute;height: 100%;display: table;z-index: -1;width: 100%;background-image: url('./Data/splashScreeeeen.jpg');background-size:cover;background-position:center;");
    			askForWebcam();
    		};
		});

		audioLoader.load( './Sounds/flyToCome.mp3', function( buffer ) {
			flyToCome.setBuffer( buffer );
			flyToCome.setRefDistance( 1 );

			scope.sequencesToLoad--;
			ord = (scope.sequencesToLoad/scope.sequences.length+3)*75;

			splashScreenMask.setAttribute("style","visibility:visible;position:absolute;height: 100%;display: table;z-index: 10;width: 100%;background-image: url('./Data/loaded.png');background-size:cover;background-position:center;clip-path: polygon(0 "+ord+"%, 100% "+ord+"%, 100% 100%, 0% 100%);");

    		if ( scope.sequencesToLoad === 0 ){
    			console.log("Everything loaded !");
    			splashScreen.setAttribute("style", "opacity:0.5;visibility:visible;position:absolute;height: 100%;display: table;z-index: -1;width: 100%;background-image: url('./Data/splashScreeeeen.jpg');background-size:cover;background-position:center;");
  				splashScreenMask.setAttribute("style","visibility:visible;position:absolute;height: 100%;display: table;z-index: 10;width: 100%;background-image: url('./Data/loaded.png');background-size:cover;background-position:center;");
    			
    			askForWebcam();
    		};
		});

		audioLoader.load( './Sounds/onlyBass.mp3', function( buffer ) {
			onlyBass.setBuffer( buffer );
			onlyBass.setRefDistance( 1 );

			scope.sequencesToLoad--;
			ord = (scope.sequencesToLoad/scope.sequences.length+3)*75;

			splashScreenMask.setAttribute("style","visibility:visible;position:absolute;height: 100%;display: table;z-index: 10;width: 100%;background-image: url('./Data/loaded.png');background-size:cover;background-position:center;clip-path: polygon(0 "+ord+"%, 100% "+ord+"%, 100% 100%, 0% 100%);");

    		if ( scope.sequencesToLoad === 0 ){
    			console.log("Everything loaded !");
    			splashScreen.setAttribute("style", "opacity:0.5;visibility:visible;position:absolute;height: 100%;display: table;z-index: -1;width: 100%;background-image: url('./Data/splashScreeeeen.jpg');background-size:cover;background-position:center;");
  				splashScreenMask.setAttribute("style","visibility:visible;position:absolute;height: 100%;display: table;z-index: 10;width: 100%;background-image: url('./Data/loaded.png');background-size:cover;background-position:center;");

    			askForWebcam();
    		};
		});


	}

	Scene.prototype.update = function (iDelta){
		//checkViewForSound();

		if ( !this.toPlay && getSceneDistance()>0.5 && getSceneDistance()<40 ){
			this.toPlay = true;
			this.sequences[0].update(iDelta);
			findMarkerMask.setAttribute("style", "visibility:hidden");
			markerExplanation.setAttribute("style", "visibility:hidden");
			markerLogo.setAttribute("style", "visibility:hidden");
			linkMarker.setAttribute("style", "visibility:hidden");
		}

		var sequences = this.sequences;
		for ( var i = 0; i < sequences.length; i++ ) {
			if (!!sequences[i].isSupposedToStart){
				this.controlStage(sequences[i].update(iDelta), iDelta);
			}
		}	
	};



	// This function manages the launch of sequences and actions considering the codes returned from the sequences
	Scene.prototype.controlStage = function (iCodes, iDelta){
		for ( var i = 0; i < iCodes.length; i++ ){
			switch(iCodes[i]) {
				case "movers":
					var sequencesConcerned = this.sequences.filter(function( obj ) {
						return obj.name == "movers" || obj.name == "proximityMan";
					});
					sequencesConcerned[0].update(iDelta);
					sequencesConcerned[1].update(iDelta);

				break;
				case "showFormURL":
					/*var obj = scene.getObjectByName( "squareTown" );
					if (obj){
						obj.visible = false;
						soundMusic.stop();
					}*/
					formURL.setAttribute("style", "visibility:visible;position:absolute;font-color:white;top:90%;left:50%;height: 10px;display: table;z-index: 100;margin-left:-158px;margin-top:-100px;font-size:15px");
				break;
				default:
					if(iCodes[i] != undefined){
						var sequencesConcerned = this.sequences.filter(function( obj ) {
							return obj.name == iCodes[i];
						});
						if(sequencesConcerned[0] != undefined){
							sequencesConcerned[0].update(iDelta);
						}		
					}
				break;
			} 
		}
	};


	/*
		The GameManager manages all scenes
	*/
	function GameManager (iGameScenesArray){
		this.gameScenesArray = iGameScenesArray;
		this.currentScene = "";
	};

	GameManager.prototype.start = function (){
		this.currentScene = this.gameScenesArray[0];
		this.gameScenesArray[0].init();
	}

	GameManager.prototype.update = function (iDelta){
		if (this.currentScene.sceneObjects)
			this.currentScene.update(iDelta);
	}


	/*Z

	var getSceneRotation = function(){
		var vect1 = new THREE.Euler();
		scene.getWorldRotation(vect1);
		vect1.reorder ('ZYX');
		cons.innerHTML="Rotation x :"+vect1._x;
		cons.append("Rotation y :"+vect1._y);
		var rotationArray = [vect1._x, vect1._y];
		return rotationArray;
	};
	*/
	var getSceneDistance = function(){
		return distanceVector( camera.getWorldPosition(), scene.getWorldPosition() );
	};

	function distanceVector( v1, v2 )
	{
	    var dx = v1.x - v2.x;
	    var dy = v1.y - v2.y;
	    var dz = v1.z - v2.z;

	    return Math.sqrt( dx * dx + dy * dy + dz * dz );
	}

	var getScenePanoramicRotation = function(){
		var vect1 = new THREE.Euler();
		camera.getWorldRotation(vect1);
		vect1.reorder ('ZYX');
		//cons.innerHTML = "<span>euler y:"+vect1.y+"+vect1.y+"+"</span>";

		//return camera.rotation.y;
		return vect1.y;
	};

	/*var checkViewForSound = function (){
		if (getSceneDistance()!=0 && !soundMusic.isPlaying)
			soundMusic.play();
	};*/


	//A dancer is an extension from a GameObject. It contains every information on a dancer.
	function Dancer(iName, iModelName, iPositionArray, iRotationArray, iScaleArray, iAnimationBoard, iCodeWhenObserved, iApparitionTime, iState, iMaxDist){
		GameObject.call (this, iName, iModelName, iPositionArray, iRotationArray, iScaleArray)
		this.isDestroyed = false;
		this.mixer = {};
		this.animations = (typeof iAnimationBoard !== 'undefined') ?  iAnimationBoard : [];
		this.codeWhenObserved = (typeof iCodeWhenObserved !== 'undefined') ?  iCodeWhenObserved : "" ;
		this.apparitionTime = (typeof iApparitionTime !== 'undefined') ?  iApparitionTime : 0.05;
		this.visible = false;
		this.state = (typeof iState !== 'undefined') ?  iState : "active";
		this.maxDist = (typeof iMaxDist !== 'undefined') ?  iMaxDist : 5;
		this.previousDistanceToVisitor = -1;
	};

	Dancer.prototype = Object.create( GameObject.prototype );
	Dancer.prototype.constructor = Dancer;

	//this function triggers every behavior described in the animation board of the dancer
	Dancer.prototype.trigger = function (iElapsed){
		anims = this.animations;
		for ( var i = 0; i < anims.length; i++ ) {
			//if (anims[i].triggerTime > 0 && anims[i].triggerTime < iElapsed && !(anims[i].anim.type == "freeze") && !anims[i].triggered){
			if (anims[i].triggerTime > -1 && anims[i].triggerTime < iElapsed && !anims[i].triggered){
				if (anims[i].anim && anims[i].anim.play){
					anims[i].anim.play();
				}
				if ( anims[i].changeState ){
					this.state = anims[i].changeState;
				}

				
				if (!!anims[i].sound){
					var obj = scene.getObjectByName( this.name );
					var sound = {};
					if (anims[i].sound == "soundMusic"){
						sound = soundMusic;
					}
					if (anims[i].sound == "flyToCome"){
						sound = flyToCome;
					}
					if (anims[i].sound == "onlyBass"){
						sound = onlyBass;
					}
					if (!!sound){
						obj.add( sound );
						sound.play();
					}
					
				}

				anims[i].triggered = true;
			}
			/*if ( anims[i].type == "freeze"){
				this.state = "outsideTime";
			}*/	
		}
	};

	//this function is used when the user wants sees a dancer or manage to "see" it
	Dancer.prototype.observe = function (){
		var obj = scene.getObjectByName( this.name );
		var scope = this;
		if (this.state == "active"){
			var animation = this.animations.filter(function( obj ) {
				return obj.type == "hello";
			});

			var sound = null;
			if (animation[0]){
				if ( animation[0].sound == "soundFly"){
					sound = soundFly;
				}
				if (animation[0] && animation[0].sound && animation[0].sound == "soundMusic"){
					sound = soundMusic;
				}				
				if (sound !== null){
					obj.add( sound );
					sound.play();
				}
							
				this.mixer.stopAllAction ();
				animation[0].anim.play();
				counterManager.addPoints(1);
				this.state = "observed";
				animation[0].anim._mixer.addEventListener('finished', function(){
					scope.state = "toDelete";
				});
			}

		}
	};

	//this function is used to change the dancer's head color as regards its state
	Dancer.prototype.updateColor = function(){
		var obj = scene.getObjectByName( this.name );
		//var whiteColor = {r:0.85, g:0.85, b:0.85};
		var whiteColor = {r:0.1, g:0.1, b:0.1};
		var greenColor = {r:0.5, g:0.7, b:0.7};
		if ( typeof obj!= 'undefined' && typeof obj.material != 'undefined'){
			switch( this.state ) {
				case "proximity":
					this.updateProximityColor ( getSceneDistance() );
				break;
				case "outsideTime":
					obj.material[0].color = greenColor;
				break;
				/*case "outsideTimeRotation":
					obj.material[0].color = whiteColor;
				break;*/
				case "passive":
					obj.material[0].color = whiteColor;
				break;
				case "active":
					obj.material[0].color = {r:0, g:0, b:0};
				break;
				case "lookAtCamera":
				break;
				case "rotateSin":
				break;
				case "readyToFindUser":
				break;
				case "toDelete":
					obj.material[0].color = whiteColor;
				break;
				case "observed":
					obj.material[0].color = whiteColor;
				break;
				default:
			}
		}
	}

	//this function is not used in this version of the game but updates the color for a dancer whose state is "proximity"
	Dancer.prototype.updateProximityColor = function (iDistance){
		var maxDist = this.maxDist;
		var obj = scene.getObjectByName( this.name );
		if ( typeof obj!= 'undefined' && typeof obj.material != 'undefined'){
			obj.material[1].color = {r:0.5, g:iDistance/maxDist, b:iDistance/maxDist};
			if (iDistance > 1.8 && iDistance < 2.2){
				this.state = "active";
				obj.material[1].color = {r:0, g:0, b:0};
				var animation = this.animations.filter(function( ob ) {
					return ob.name == "activate";
				});
				//animation[0].anim.play();
			}
		}			
	};

	//This function is used to move the telescop 
	Dancer.prototype.animRotate = function ( iTryToFindUser){
		var i3DObj = scene.getObjectByName( this.name );
		var codeToReturn ="";
		var step = 1;
		if (i3DObj){
			i3DObj.rotation.y += Math.abs(Math.sin(x)/50);
			i3DObj.rotation.y = i3DObj.rotation.y%6.28;
			x+=0.01;

			if (x>1000){
				x = x%3.14;
			}

			if (iTryToFindUser){
				var borne = camera.rotation.y;
				if (borne<0){
					borne += 3.14/2;
				}
				else {
					borne -= 3.14/2;
				}

				if (borne - 0.1 <(i3DObj.rotation.y - 3.14)/2 && (i3DObj.rotation.y - 3.14)/2 < borne + 0.1){
					var animation = this.animations.filter(function( obj ) {
						return obj.type == "sawYou";
					});
					if (animation[0] && animation[0].anim && animation[0].anim.play){
						animation[0].anim.play();
					}
					this.state = "lookAtCamera";
					pointer.setAttribute("style", "visibility:visible;position:absolute;top:50%;left:50%;height: 10px;display: table;z-index: 3;width: 10px;border:1.5px solid white;border-radius:10px;margin-left:-5px;margin-top:-5px;");
					counter.setAttribute("style", "visibility:visible;color:white;font-size:1.6em;font-weight:bold;position:absolute;top:10%;left:10%;height: 80px;width: 80px;display: table;z-index: 3;margin-left:-25px;margin-top:-25px;");
					document.getElementById("imgCounter").setAttribute("style", "margin-bottom:-5px ");
					codeToReturn = "coasterMan";
					//codeToReturn = "moversRot1";
				}
			}	
		}
		return codeToReturn;

	}

	//this function makes the telescop look at the user
	Dancer.prototype.lookAtCamera = function (){
		var obj = scene.getObjectByName( this.name );
		if ( typeof obj!= 'undefined'){
			obj.lookAt(camera.position);
			obj.rotateX(0.5);
		}
	};

	//todo fix this function
	Dancer.prototype.fadeOut = function (){
		var obj = scene.getObjectByName( this.name );
		if ( typeof obj != 'undefined'){
			obj.material.transparent = true;
			object.material.opacity = 0.5;
		}
	};

	//this function scrolls on a dancer's animation considering the smartphone distance to this dancer
	Dancer.prototype.moveAccordingToDistance = function (){
		var maxDist = this.maxDist;
		var v = this.positionArray;
		var iDistance = distanceVector( camera.getWorldPosition(), new THREE.Vector3( v[0], v[1], v[2] ));
		//cons.innerHTML = "<span>Distance:"+iDistance+"</span>";
		if (iDistance> 1.2 && iDistance <maxDist){

			var animation = this.animations.filter(function( obj ) {
				return obj.type == "freeze";
			});
			var animDuration = animation[0].anim._clip.duration;
			
			if ( this.previousDistanceToVisitor < 0){
				this.previousDistanceToVisitor = iDistance;
				//this.mixer.time = 0;
				animation[0].anim.play();
			}
			else{
				if (iDistance>maxDist){
					iDistance = maxDist;
				}		

				var timeToSet = animDuration - (iDistance/maxDist)*animDuration;
				var deltaToSet = timeToSet - this.mixer.time;

				if ( ((this.mixer.time+deltaToSet) > animDuration-0.01) || ((this.mixer.time+deltaToSet) < 0)){
					this.mixer.update(0);
				}
				else {
					this.mixer.update(deltaToSet);
				}			
			}
		}
	};

	//this function frees the dancer from its link to user's distance to him
	Dancer.prototype.checkToDeliverFromProximity = function (){
		var v = this.positionArray;
		var iDistance = distanceVector( camera.getWorldPosition(), new THREE.Vector3( v[0], v[1], v[2] ));
		var delta = 1;
		if (0.01<iDistance && iDistance<1.5){
			this.state = "active";
			this.observe();
		}
	};

	/*Dancer.prototype.moveAccordingToPanoramicRotation = function ( iPanoramicRotation ){

		if (iPanoramicRotation> 0 && iPanoramicRotation <2.2){
			var animation = this.animations.filter(function( obj ) {
				return obj.type == "freeze";
			});
			var animDuration = animation[0].anim._clip.duration;
			
			if ( this.previousDistanceToVisitor < 0){
				this.previousDistanceToVisitor = iPanoramicRotation;
				//this.mixer.time = 0;
				animation[0].anim.play();
			}
			else{
				var maxDist = 2.2;
				if (iPanoramicRotation>maxDist){
					iPanoramicRotation = maxDist;
				}		

				var timeToSet = animDuration - (iPanoramicRotation/maxDist)*animDuration;
				var deltaToSet = timeToSet - this.mixer.time;

				if ( ((this.mixer.time+deltaToSet) > animDuration-0.01) || ((this.mixer.time+deltaToSet) < 0)){
					this.mixer.update(0);
				}
				else {
					this.mixer.update(deltaToSet);
				}			
			}
		}
	};

	Dancer.prototype.checkToDeliverFromRotation = function ( iPanoramicRotation ){
		var delta = 0.10;
		// console.log(iPanoramicRotation);
		if (this.rotDis-delta<iPanoramicRotation && iPanoramicRotation<this.rotDis+delta){
			console.log("Delivered from rotation");
			//this.state = "active";
			//this.observe();
		}
	};*/


	Dancer.prototype.destroy = function ( iDistance ){
		var obj = scene.getObjectByName( this.name );
		scene.remove(obj);
	};


	//the counter indicates the score of the player
	function CounterManager (iDOMCounter){
		this.iDOMCounter = iDOMCounter;
		this.iDOMScore = document.getElementById("score");
		this.count = 0;
	};

	CounterManager.prototype.addPoints = function (iPointsToAdd){
		this.count+=iPointsToAdd;
		this.iDOMScore.innerHTML = this.count;
	}

	//a sequence is a set of dancers
	function Sequence (iName, iDancersList, iType, iNextSequence, iPosition, iMusicFileName, iDuration){
		this.name = iName;
		this.timer = {};
		this.dancers = iDancersList;
		this.type = (typeof iType !== 'undefined') ?  iType : "oneTouch";
		this.nextSequence = (typeof iNextSequence !== 'undefined') ?  iNextSequence : ["", 100];
		this.duration = (typeof iDuration !== 'undefined') ?  iDuration : "none";
		this.position = (typeof iPosition !== 'undefined') ?  iPosition : [0, 0, 0] ;
		this.musicFileName = (typeof iMusicFileName !== 'undefined') ?  iMusicFileName : "none";
		this.toLoad = -1;
		this.everythingLoaded = false;
		this.isLoading = false;
		this.isSupposedToStart = false;
	};

	//this function is called asynchronously to load every model from blender exports
	Sequence.prototype.load = function(iCallBack){
		this.isLoading = true;
		this.toLoad = this.dancers.length;
		var scope2 = this;

		for ( var j = 0; j < scope2.dancers.length; j++ ) {

			var loader = new THREE.JSONLoader();
			var gameObject = scope2.dancers[j];


			if (gameObject instanceof Dancer){
				loader.load(j, './Models/'+scope2.dancers[j].modelName+'.json', function(geometry, materials, u) {
					materials.forEach(function (material) {
					  material.skinning = true;
					});
					var character = new THREE.SkinnedMesh(
					  geometry,
					  new THREE.MeshFaceMaterial(materials)
					);

					var mixer = new THREE.AnimationMixer(character);
					var animations = [];
					character.geometry.computeVertexNormals();

					for (var k = 0; k < geometry.animations.length; k++)
					{
						animations[k] = mixer.clipAction(geometry.animations[ k ]);
						animations[k].setEffectiveWeight(1);
						animations[k].setLoop(THREE.LoopOnce, 0);
						animations[k].clampWhenFinished = true;
						animations[k].enabled = true;	
					}

		    		// var gameObject = scope2.dancers[(scope2.dancers.length-scope2.toLoad)];
		    		var gameObject = scope2.dancers[u];
		    		character.name = gameObject.name;
		    		character.visible = false;
		    		scene.add(character);

		    		gameObject.id = character.id;
		    		gameObject.mixer = mixer;

					for (var l = 0; l < geometry.animations.length; l++)
					{
						gameObject.animations[l].anim = animations[ l ];
						if ( !!gameObject.animations[l].loop ){
							gameObject.animations[l].anim.setLoop(THREE.LoopRepeat);
						}
					}

	    			character.scale.set( gameObject.scaleArray[0], gameObject.scaleArray[1], gameObject.scaleArray[2] );
		    		character.rotateX( gameObject.rotationArray[0] );
		    		character.rotateY( gameObject.rotationArray[1] );
		    		character.rotateZ( gameObject.rotationArray[2] );
		    		character.position.set( gameObject.positionArray[0], gameObject.positionArray[1], gameObject.positionArray[2] );
		    		scope2.toLoad--;
		    		if ( scope2.toLoad === 0 ){
		    			scope2.everythingLoaded = true;
		    			iCallBack(scope2.name);

		    		};
				});
			}

			else{
	    		loader.load(j, './Models/'+gameObject.modelName+'.json', function(geometry, materials, m) {
					var material = new THREE.MeshPhongMaterial( { 
						color: 0x8cc5eb , 
						specular: 0x808080 , 
						shininess: 10,
						transparent:true,
		    			clipShadows: true 
					});

		    		//var obj3D = new THREE.Mesh(geometry, new THREE.MeshFaceMaterial(materials));

		    		//var gameObject = scope2.sceneObjects[(scope2.sceneObjects.length-scope2.toLoad)];
		    		var gameObject = scope2.sceneObjects[m]
		    		var obj3D = new THREE.Mesh(geometry, material);
		    		obj3D.name = gameObject.name;
		    		obj3D.visible = false;
		    		scene.add(obj3D);

		    		gameObject.id = obj3D.id;

	    			obj3D.scale.set( gameObject.scaleArray[0], gameObject.scaleArray[1], gameObject.scaleArray[2] );
		    		obj3D.rotateX( gameObject.rotationArray[0] );
		    		obj3D.rotateY( gameObject.rotationArray[1] );
		    		obj3D.rotateZ( gameObject.rotationArray[2] );
		    		obj3D.position.set( gameObject.positionArray[0], gameObject.positionArray[1], gameObject.positionArray[2] );
		    		scope2.toLoad--;
		    		if ( scope2.toLoad === 0 ){
		    			scope2.everythingLoaded = true;
		    			iCallBack(scope2.name);
		    		};
	    		});
			}

		}
	}


	/**
		This is the function called at every tick, it spreads updates amongs its dancers. It is also used for raycasting and multiple dancer "observation".
	**/

	Sequence.prototype.update = function (iDelta){
		this.isSupposedToStart = true;
		var codesToReturnToScene = [];

		if (!this.timer.running){
			this.timer = new THREE.Clock ( true );
			this.timer.start();
		}

		else {

			if (this.timer.running){
				var elapsed = this.timer.getElapsedTime();
				codesToReturnToScene.push(this.triggerNextSequence(elapsed));
				for ( var i = 0; i < this.dancers.length; i++ ){
					if (this.dancers[i] instanceof Dancer){
						if ( this.dancers[i].mixer.update && !(this.dancers[i].state === "outsideTime" || this.dancers[i].state === "outsideTimeRotation" ))
							this.dancers[i].mixer.update(iDelta);
						if ( this.dancers[i].apparitionTime < elapsed && !this.dancers[i].visible ){
							this.dancers[i].display();
							this.dancers[i].visible = true;
						}
						if ( this.dancers[i].apparitionTime < elapsed )
							this.dancers[i].trigger(elapsed);

						this.dancers[i].updateColor();
						switch( this.dancers[i].state ) {
							case "outsideTime":
								//this.dancers[i].moveAccordingToDistance ( getSceneDistance() );
								//this.dancers[i].checkToDeliverFromProximity( getSceneDistance() );
								this.dancers[i].moveAccordingToDistance();
								this.dancers[i].checkToDeliverFromProximity();


							break;
							/*case "outsideTimeRotation":
								this.dancers[i].moveAccordingToPanoramicRotation ( getScenePanoramicRotation() + 1.21 );
								this.dancers[i].checkToDeliverFromRotation( getSceneDistance() + 1.21);
							break;*/
							case "lookAtCamera":
								this.dancers[i].lookAtCamera ();
							break;
							case "rotateSin":
								this.dancers[i].animRotate ();
							break;
							case "readyToFindUser":
								codesToReturnToScene.push(this.dancers[i].animRotate (true));
							break;
							case "toDelete":
								codesToReturnToScene.push(this.dancers[i].codeWhenObserved);
								this.dancers[i].destroy();
								delete this.dancers[i];
							break;
							default:
						}
					}
				}
			}
			


			var vector = new THREE.Vector3(0, 0, -1);
            vector = camera.localToWorld(vector);
            vector.sub(camera.position);
            var raycaster = new THREE.Raycaster( camera.position, vector);
			var intersects = raycaster.intersectObjects( scene.children );
			var seenDancers = [];
			var add = true;
			if (intersects.length>0){
				for ( var i = 0; i < intersects.length; i++ ) {
					//cons.innerHTML= intersects[i].object.name;

					var intersectedGameObjects = this.dancers.filter(function( obj ) {
							return obj.id == intersects[i].object.id;
						});

					if ( intersectedGameObjects[0] && intersectedGameObjects[0] instanceof Dancer ){
						add = true;
						for ( var k = 0; k < seenDancers.length; k++ ){
							if ( seenDancers[k].id == intersectedGameObjects[0].id){
								add = false;
							}
						}
						if ( !!add ){
							seenDancers.push(intersectedGameObjects[0]);
						}
					}
				}
				if ( this.type == "multiple" ){
					if ( seenDancers.length == this.dancers.length )
					{
						for ( var j = 0; j < seenDancers.length; j++ ){
							seenDancers[j].observe();
						}
					}
				}
				else {
					for ( var j = 0; j < seenDancers.length; j++ ){
						seenDancers[j].observe();
					}
				}
			}
			return codesToReturnToScene;
		}
	}


	Sequence.prototype.destroy = function (){
		for ( var i = 0; i < this.dancers.length; i++ ){
			this.dancers[i].destroy();
			delete this.dancers[i];
		}
		this.timer.stop();
	}

	Sequence.prototype.triggerNextSequence = function (iElapsed){
		if (this.nextSequence[0] > 0 && this.nextSequence[0] < iElapsed && this.nextSequence[1]!==""){
			var sequenceToReturn = copy(this.nextSequence[1]);
			this.nextSequence[0] = -1;
			this.nextSequence[1] = "";
			return sequenceToReturn;
		}
	};

	var copy = function (iObject){
		return JSON.parse(JSON.stringify(iObject));
	}


	//***Initialisation***
	//This part of the code is the creation of every object, animation board, dancer, sequences and scenes that will be playable in the game.

	var squareTown = new GameObject ( "squareTown", "townJoined", [0, 0, 0], [0, 0, 0], [0.1, 0.1, 0.1] );
	var telescopStructure = new GameObject ( "telescopStructure", "telescopStructure", [0, 0, 0], [0, 0, 0], [0.1, 0.1, 0.1] );
	//var squareTown = new GameObject ( "table2", "table2", [0, 0, 0], [0, 0, 0], [0.1, 0.1, 0.1] );
	//var squareTown = new GameObject ( "jul2", "jul2", [0, 0, 0], [0, 0, 0], [1, 1, 1] );


	var moversAnimBoard = [
		{
			name:"fall",
			triggerTime:-1,
			anim:{},
			type: "freeze",
			sound: "soundFly"
		},
		{
			name:"walk",
			triggerTime:-1,
			anim:{}
		},
		{
			name:"hello",
			triggerTime:-1,
			anim:{}
		},
		{
			name:"static",
			triggerTime:-1,
			anim:{}
		}

	];


	var animBoardBike = [
		{
			name:"hit",
			triggerTime:2,
			anim:{}
		}
	];

	var animBoardTelescop = [
		{
			name:"nod",
			triggerTime:-1,
			anim:{},
			type: "sawYou"

		},
		{
			name:"idle",
			triggerTime:4,
			anim:{},
			changeState: "readyToFindUser",
			triggered : false
		}
	];

	/*var animBoardTelescopeChair = [
		{
			name:"idle",
			triggerTime:4,
			//7
			anim:{},
			changeState: "readyToFindUser",
			triggered : false
		}
	];*/

	var animBoardTelescopeChair = [
		{
			name:"static",
			anim:{}
		},
		{	name:"idle",
			triggerTime:4,
			//7
			anim:{},
			changeState: "readyToFindUser",
			triggered : false
		}
	];

	/*var scientistBoard = [
		{
			name:"sat",
			triggerTime:0,
			anim:{},
			loop: true
		},
		{
			name:"static",
			anim:{},
			triggerTime:4,
			changeState: "readyToFindUser",
			triggered : false
		}

	];*/

	var scientistFixedBoard = [		
		{
			name:"idle",
			triggerTime:-1,
			anim:{}
		}
	];

	var animBoard = [
		{
			name:"sat",
			triggerTime:0.01,
			anim:{},
			loop: true
		},
		{
			name:"static",
			anim:{},
			triggerTime:-1
		},
		{
			name:"static",
			anim:{},
			triggerTime:-1
		},
		{
			name:"static",
			anim:{},
			triggerTime:-1
		},
		{
			name:"static",
			anim:{},
			triggerTime:-1
		}

	];

	var paperCoasterBoard = [
		{
			name:"getAttention",
			triggerTime:6,
			anim:{},
			changeState: "active"
		},
		{
			name:"Seen",
			type: "hello",
			triggerTime:-1,
			anim:{},
			sound: "soundMusic"
		},
		{
			name:"coasterFlight",
			//triggerTime:0.01,
			triggerTime:0.01,
			anim:{}
		},
		{
			name:"fromSides",
			//triggerTime:0.01,
			triggerTime:-1,
			anim:{}
		},
		{
			name:"idle",
			anim:{},
			triggerTime:-1
		},
		{
			triggerTime:1,
			sound: "flyToCome"
		}
	];

	var eightDancers1Board = [
		{
			name:"getAttention",
			triggerTime:0.1,
			anim:{},
			changeState: "active"
		},
		{
			name:"Seen",
			type: "hello",
			triggerTime:-1,
			anim:{},
			sound:"soundFly"
		},
		{
			anim:{},
			triggerTime:-1
		},
		{
			anim:{},
			triggerTime:-1
		},
		{
			name:"idle",
			anim:{},
			triggerTime:-1
		},
		{
			name:"",
			triggerTime:6,
			anim:{},
			changeState: "toDelete"
		}

	];

	/*var fromRoofBoard = [
		{
			name:"getAttention",
			triggerTime:-1,
			anim:{},
			changeState: "active"
		},
		{
			name:"Seen",
			type: "hello",
			triggerTime:-1,
			anim:{},
			sound:"soundFly"
		},
		{
			name:"coasterFlight",
			triggerTime:-1,
			anim:{}
		},
		{
			name:"fromRoof",
			//triggerTime:0.01,
			triggerTime:2,
			anim:{}
		},
		{
			name:"fromSides",
			triggerTime:-1,
			anim:{}
		},
		{
			name:"idle",
			anim:{},
			triggerTime:-1
		}
	];*/

var fromRoofBoard = [
		{
			name:"getAttention",
			triggerTime:2.2,
			anim:{},
			changeState: "active"
		},
		{
			name:"Seen",
			type: "hello",
			triggerTime:-1,
			anim:{},
			sound:"soundFly"
		},
		{
			name:"fromRoof",
			triggerTime:0,
			anim:{}
		},
		{
			name:"idle",
			anim:{},
			triggerTime:-1
		},
		{
			name:"",
			triggerTime:5,
			anim:{},
			changeState: "toDelete"
		}
	];

	var fromSidesBoard = [
		{
			name:"getAttention",
			triggerTime:-1,
			anim:{},
		},
		{
			name:"Seen",
			type: "hello",
			triggerTime:-1,
			anim:{},
			sound:"soundFly"
		},
		{
			name:"coasterFlight",
			triggerTime:-1,
			anim:{}
		},
		{
			name:"fromSides",
			triggerTime:0.01,
			anim:{}
		},
		{
			name:"idle",
			anim:{},
			triggerTime:-1
		},
		{
			name:"",
			triggerTime:2.3,
			anim:{},
			changeState: "toDelete"
		}
	];


	var moversBoard = [
		{
			name:"flyToCircle",
			triggerTime:-1,
			anim:{},
			type: "freeze"
		},
		{
			name:"explode",
			type: "hello",
			triggerTime:-1,
			anim:{}//,
			//sound:"soundMusic"
		},
		{
			name:"idle",
			anim:{},
			triggerTime:-1
		},
		{
			triggerTime:0,
			anim:{},
			sound:"onlyBass"
		}
	];

	var moversBoardWithDeletion = [
		{
			name:"flyToCircle",
			triggerTime:-1,
			anim:{},
			type: "freeze"
		},
		{
			name:"explode",
			type: "hello",
			triggerTime:-1,
			anim:{}
		},
		{
			name:"idle",
			anim:{},
			triggerTime:-1
		},
		{
			name:"",
			triggerTime:7,
			anim:{},
			changeState: "toDelete"
		}
	];

	var moversBoardPro = [
		{
			name:"flyToCircle",
			triggerTime:-1,
			anim:{},
			type: "freeze"
		},
		{
			name:"explode",
			type: "hello",
			triggerTime:-1,
			anim:{}
		},
		{
			name:"idle",
			anim:{},
			triggerTime:-1
		}
	];


	var secondsPerBeat = 0.5454;
	var scaleDancers = [0.075, 0.075, 0.075];

	//var scientist = new Dancer( "scientist", "ScientistFixed", [-0.40, 0.50, -0.41], [0, 0, 0], scaleDancers, scientistFixedBoard, "", 0.01, "rotateSin" );
	var telescop = new Dancer( "telescop", "telescop", [-0.40, 0.47, -0.41], [0, 0, 0], [0.04, 0.04, 0.04], animBoardTelescop,"", 0.1, "rotateSin" );
	var telescopeChair = new Dancer( "TelescopeChair", "telescopChairAndScientst", [-0.40, 0.47, -0.41], [0, 0, 0], [0.04, 0.04, 0.04], animBoardTelescopeChair,"", 0.1, "rotateSin" );
    //var introScene = new Sequence ("introScene", [telescop, telescopeChair, scientist]);
    var introScene = new Sequence ("introScene", [telescop, telescopeChair]);

    /*var bike = new Dancer( "bike", "BikeJoined", [0, 0.4, 0], [ 0, -1.57, 0], scaleDancers, animBoardBike, "", 0.01, "passive" );*/
	
	var coasterDancer = new Dancer( "arrowAnim3", "PaperDancerCoaster", [0.1, 0.2, 0.2], [ 0, 0, 0], scaleDancers, paperCoasterBoard, "fromRoofDancers1", 0.3, "passive" );

	var coasterMan = new Sequence ( "coasterMan", [coasterDancer]);

	
	/*var eightDancers1TimeOffset = -0.7;
	var eD1_1 = new Dancer( "eightDancers1_1", "PaperDancerCoaster", [-0.2, 0.1,  0.4], [ 0, 0, 0], scaleDancers, copy(eightDancers1Board),"", eightDancers1TimeOffset+0*secondsPerBeat, "active" );
	var eD1_2 = new Dancer( "eightDancers1_2", "PaperDancerCoaster", [-0.0, 0.1,  0.4], [ 0, 0, 0], scaleDancers, copy(eightDancers1Board),"", eightDancers1TimeOffset+1*secondsPerBeat, "active" );
	var eD1_3 = new Dancer( "eightDancers1_3", "PaperDancerCoaster", [ 0.2, 0.1,  0.4], [ 0, 0, 0], scaleDancers, copy(eightDancers1Board),"", eightDancers1TimeOffset+2*secondsPerBeat, "active" );
	var eD1_4 = new Dancer( "eightDancers1_4", "PaperDancerCoaster", [-0.2, 0.1, -0.4], [ 0, 3.14, 0], scaleDancers, copy(eightDancers1Board),"", eightDancers1TimeOffset+3*secondsPerBeat, "active" );
	var eD1_5 = new Dancer( "eightDancers1_5", "PaperDancerCoaster", [ 0.0, 0.1, -0.4], [ 0, 3.14, 0], scaleDancers, copy(eightDancers1Board),"", eightDancers1TimeOffset+4*secondsPerBeat, "active" );
	var eD1_6 = new Dancer( "eightDancers1_6", "PaperDancerCoaster", [ 0.2, 0.1, -0.4], [ 0, 3.14, 0], scaleDancers, copy(eightDancers1Board),"", eightDancers1TimeOffset+5*secondsPerBeat, "active" );
	var eD1_7 = new Dancer( "eightDancers1_7", "PaperDancerCoaster", [ 0.3, 0.1, -0.1], [ 0, 1.57, 0], scaleDancers, copy(eightDancers1Board),"", eightDancers1TimeOffset+11*secondsPerBeat/2, "active" );
	var eD1_8 = new Dancer( "eightDancers1_8", "PaperDancerCoaster", [ 0.3, 0.1,  0.1], [ 0, 1.57, 0], scaleDancers, copy(eightDancers1Board),"", eightDancers1TimeOffset+6*secondsPerBeat, "active" );
	var eightDancers1 = new Sequence ( "eightDancers1", [eD1_1, eD1_2, eD1_3, eD1_4, eD1_5, eD1_6, eD1_7, eD1_8], "", [2.6, "eightDancers2"]);
*/

	var fromRoofDancers1TimeOffset = 0;
	var fRD1_1 = new Dancer( "fromRoofDancers1_1", "PaperDancerFromRoof", [-0.4, 0.2,  0.3], [ 0, 0, 0], scaleDancers, copy(fromRoofBoard),"", fromRoofDancers1TimeOffset+0*secondsPerBeat, "passive" );
	var fRD1_2 = new Dancer( "fromRoofDancers1_2", "PaperDancerFromRoof", [-0.4, 0.2,  0.1], [ 0, 0, 0], scaleDancers, copy(fromRoofBoard),"", fromRoofDancers1TimeOffset+1*secondsPerBeat, "passive" );
	var fRD1_3 = new Dancer( "fromRoofDancers1_3", "PaperDancerFromRoof", [ 0.4, 0.2, -0.1], [ 0, 0, 0], scaleDancers, copy(fromRoofBoard),"", fromRoofDancers1TimeOffset+2*secondsPerBeat, "passive" );
	var fRD1_4 = new Dancer( "fromRoofDancers1_4", "PaperDancerFromRoof", [ 0.4, 0.2, -0.3], [ 0, 0, 0], scaleDancers, copy(fromRoofBoard),"", fromRoofDancers1TimeOffset+3*secondsPerBeat, "passive" );
	var fromRoofDancers1 = new Sequence ( "fromRoofDancers1", [fRD1_1, fRD1_2, fRD1_3, fRD1_4], "", [3, "fromRoofDancers2"]);

	var fromRoofDancers2TimeOffset = 0;
	var fRD2_1 = new Dancer( "fromRoofDancers2_1", "PaperDancerFromRoof", [-0.5, 0.2,  0.3], [ 0, 0, 0], scaleDancers, copy(fromRoofBoard),"", fromRoofDancers2TimeOffset+0*secondsPerBeat, "passive" );
	var fRD2_2 = new Dancer( "fromRoofDancers2_2", "PaperDancerFromRoof", [-0.5, 0.2,  0.1], [ 0, 0, 0], scaleDancers, copy(fromRoofBoard),"", fromRoofDancers2TimeOffset+1*secondsPerBeat, "passive" );
	var fRD2_3 = new Dancer( "fromRoofDancers2_3", "PaperDancerFromRoof", [-0.5, 0.2, -0.1], [ 0, 0, 0], scaleDancers, copy(fromRoofBoard),"", fromRoofDancers2TimeOffset+2*secondsPerBeat, "passive" );
	var fRD2_4 = new Dancer( "fromRoofDancers2_4", "PaperDancerFromRoof", [-0.5, 0.2, -0.3], [ 0, 0, 0], scaleDancers, copy(fromRoofBoard),"", fromRoofDancers2TimeOffset+3*secondsPerBeat, "passive" );
	var fromRoofDancers2 = new Sequence ( "fromRoofDancers2", [fRD2_1, fRD2_2, fRD2_3, fRD2_4], "", [4.5, "eightDancers1"]);

	var eightDancers1TimeOffset = 0;
	var eD1_1 = new Dancer( "eightDancers1_1", "PaperDancerCoaster", [-0.2, 0.1,  0.4], [ 0, 0, 0], scaleDancers, copy(eightDancers1Board),"", eightDancers1TimeOffset+0*secondsPerBeat/2, "active" );
	var eD1_2 = new Dancer( "eightDancers1_2", "PaperDancerCoaster", [-0.0, 0.1,  0.4], [ 0, 0, 0], scaleDancers, copy(eightDancers1Board),"", eightDancers1TimeOffset+1*secondsPerBeat/2, "active" );
	var eD1_3 = new Dancer( "eightDancers1_3", "PaperDancerCoaster", [ 0.2, 0.1,  0.4], [ 0, 0, 0], scaleDancers, copy(eightDancers1Board),"", eightDancers1TimeOffset+2*secondsPerBeat/2, "active" );
	var eD1_4 = new Dancer( "eightDancers1_4", "PaperDancerCoaster", [-0.2, 0.1, -0.4], [ 0, 3.14, 0], scaleDancers, copy(eightDancers1Board),"", eightDancers1TimeOffset+3*secondsPerBeat/2, "active" );
	var eD1_5 = new Dancer( "eightDancers1_5", "PaperDancerCoaster", [ 0.0, 0.1, -0.4], [ 0, 3.14, 0], scaleDancers, copy(eightDancers1Board),"", eightDancers1TimeOffset+4*secondsPerBeat/2, "active" );
	var eD1_6 = new Dancer( "eightDancers1_6", "PaperDancerCoaster", [ 0.2, 0.1, -0.4], [ 0, 3.14, 0], scaleDancers, copy(eightDancers1Board),"", eightDancers1TimeOffset+5*secondsPerBeat/2, "active" );
	var eD1_7 = new Dancer( "eightDancers1_7", "PaperDancerCoaster", [ 0.3, 0.1, -0.1], [ 0, 1.57, 0], scaleDancers, copy(eightDancers1Board),"", eightDancers1TimeOffset+11*secondsPerBeat/2, "active" );
	var eD1_8 = new Dancer( "eightDancers1_8", "PaperDancerCoaster", [ 0.3, 0.1,  0.1], [ 0, 1.57, 0], scaleDancers, copy(eightDancers1Board),"", eightDancers1TimeOffset+6*secondsPerBeat/2, "active" );
	var eightDancers1 = new Sequence ( "eightDancers1", [eD1_1, eD1_2, eD1_3, eD1_4, eD1_5, eD1_6, eD1_7, eD1_8], "", [4.5, "eightDancers2"]);

	var eightDancers2TimeOffset = 0;
	var eD2_1 = new Dancer( "eightDancers2_1", "PaperDancerCoaster", [-0.2, 0.1,  0.5], [ 0, 0, 0], scaleDancers, copy(eightDancers1Board),"", eightDancers2TimeOffset+0*secondsPerBeat/2, "active" );
	var eD2_2 = new Dancer( "eightDancers2_2", "PaperDancerCoaster", [-0.0, 0.1,  0.5], [ 0, 0, 0], scaleDancers, copy(eightDancers1Board),"", eightDancers2TimeOffset+1*secondsPerBeat/2, "active" );
	var eD2_3 = new Dancer( "eightDancers2_3", "PaperDancerCoaster", [ 0.2, 0.1,  0.5], [ 0, 0, 0], scaleDancers, copy(eightDancers1Board),"", eightDancers2TimeOffset+2*secondsPerBeat/2, "active" );
	var eD2_4 = new Dancer( "eightDancers2_4", "PaperDancerCoaster", [-0.2, 0.1, -0.5], [ 0, 3.14, 0], scaleDancers, copy(eightDancers1Board),"", eightDancers2TimeOffset+3*secondsPerBeat/2, "active" );
	var eD2_5 = new Dancer( "eightDancers2_5", "PaperDancerCoaster", [ 0.0, 0.1, -0.5], [ 0, 3.14, 0], scaleDancers, copy(eightDancers1Board),"", eightDancers2TimeOffset+4*secondsPerBeat/2, "active" );
	var eD2_6 = new Dancer( "eightDancers2_6", "PaperDancerCoaster", [ 0.2, 0.1, -0.5], [ 0, 3.14, 0], scaleDancers, copy(eightDancers1Board),"", eightDancers2TimeOffset+5*secondsPerBeat/2, "active" );
	var eD2_7 = new Dancer( "eightDancers2_7", "PaperDancerCoaster", [ 0.3, 0.1, -0.2], [ 0, 1.57, 0], scaleDancers, copy(eightDancers1Board),"", eightDancers2TimeOffset+6*secondsPerBeat/2, "active" );
	var eD2_8 = new Dancer( "eightDancers2_8", "PaperDancerCoaster", [ 0.3, 0.1,  0.2], [ 0, 1.57, 0], scaleDancers, copy(eightDancers1Board),"", eightDancers2TimeOffset+7*secondsPerBeat/2, "active" );
	var eightDancers2 = new Sequence ( "eightDancers2", [eD2_1, eD2_2, eD2_3, eD2_4, eD2_5, eD2_6, eD2_7, eD2_8], "", [3.9, "fromSides1"]);

	var fromSidesOffset = 0;
	var fSD1_1 = new Dancer( "fromSidesDancer1", "PaperDancerCoaster", [-0.2, 0.1,  1.3], [ 0, 0   , 0], scaleDancers, copy(fromSidesBoard),"", fromSidesOffset+0*secondsPerBeat, "passive" );
	var fromSidesDancers1 = new Sequence ( "fromSides1", [fSD1_1], "", [2.2, "fromSides2"]);	

	var fSD2_1 = new Dancer( "fromSidesDancer2", "PaperDancerCoaster", [ 0.3, 0.1, 	-1], [ 0, 3.14, 0], scaleDancers, copy(fromSidesBoard),"", fromSidesOffset+0*secondsPerBeat, "passive" );
	var fromSidesDancers2 = new Sequence ( "fromSides2", [fSD2_1], "", [2.6, "plenty"]);

	var plentyOfMenArray = [];
	var spiraleLength = 13;
	for (var i = 0; i <= spiraleLength; i++) {
		var time = 0+(i*secondsPerBeat/4);
		var y = 0.2+0.04*i;
		var r = 0.3+0.03*i;
		var theta = i*3.14/7;
		var x = r*Math.cos(theta);
		var z = r*Math.sin(theta);
		var dancer = new Dancer( "plenty"+i, "PaperDancerCoaster", [x, y, z], [ 0, -theta, 0], scaleDancers, copy(eightDancers1Board),"", time, "active" );
		if (i == spiraleLength){
			dancer = new Dancer( "plenty"+i, "PaperDancerCoaster", [x, y, z], [ 0, -theta, 0], scaleDancers, copy(eightDancers1Board),"", time, "active" );
		}
		plentyOfMenArray.push(dancer);
	}
	var plenty = new Sequence ( "plenty", plentyOfMenArray, "", [6, "moversRot1"]);
	
	var moverRot1_1 = new Dancer( "moverRot1_1", "PaperDancerForMoves", [0.7, 0.8,  1], [0.9,    0, 0], scaleDancers, moversBoard, "", 0.01, "outsideTime", 6 );
	var moverRot1_2 = new Dancer( "moverRot1_2", "PaperDancerForMoves", [0.7, 0.8,  1], [0.9, 1.50, 0], scaleDancers, copy(moversBoard), "", 0.01, "outsideTime", 6 );
	var moverRot1_3 = new Dancer( "moverRot1_3", "PaperDancerForMoves", [0.7, 0.8,  1], [0.9, 3.14, 0], scaleDancers, copy(moversBoard), "", 0.01, "outsideTime", 6 );
	var moverRot1_4 = new Dancer( "moverRot1_4", "PaperDancerForMoves", [0.7, 0.8,  1], [0.9,-1.50, 0], scaleDancers, copy(moversBoard), "moversRot2", 0.01, "outsideTime", 6 );
	var moversRot1 = new Sequence ( "moversRot1", [moverRot1_1, moverRot1_2, moverRot1_3, moverRot1_4], "");

	var moverRot2_1 = new Dancer( "moverRot2_1", "PaperDancerForMoves", [0.7, 0.8,  -1], [-0.9,    0, 0], scaleDancers, copy(moversBoardWithDeletion), "", 0.01, "outsideTime" );
	var moverRot2_2 = new Dancer( "moverRot2_2", "PaperDancerForMoves", [0.7, 0.8,  -1], [-0.9, 1.50, 0], scaleDancers, copy(moversBoardWithDeletion), "", 0.01, "outsideTime" );
	var moverRot2_3 = new Dancer( "moverRot2_3", "PaperDancerForMoves", [0.7, 0.8,  -1], [-0.9, 3.14, 0], scaleDancers, copy(moversBoardWithDeletion), "", 0.01, "outsideTime" );
	var moverRot2_4 = new Dancer( "moverRot2_4", "PaperDancerForMoves", [0.7, 0.8,  -1], [-0.9,-1.50, 0], scaleDancers, copy(moversBoardWithDeletion), "", 0.01, "outsideTime" );
	var moversRot2 = new Sequence ( "moversRot2", [moverRot2_1, moverRot2_2, moverRot2_3, moverRot2_4], "", [7, "moversPro1"]);

	var moverPro1_1 = new Dancer( "moverPro1_1", "PaperDancerForMoves", [0, 0.6,  0], [3.14,    0, 0], scaleDancers, copy(moversBoardPro), "", 0.01, "outsideTime", 4.5 );
	var moverPro1_2 = new Dancer( "moverPro1_2", "PaperDancerForMoves", [0, 0.6,  0], [3.14, 1.50, 0], scaleDancers, copy(moversBoardPro), "", 0.01, "outsideTime", 4.5 );
	var moverPro1_3 = new Dancer( "moverPro1_3", "PaperDancerForMoves", [0, 0.6,  0], [3.14, 3.14, 0], scaleDancers, copy(moversBoardPro), "", 0.01, "outsideTime", 4.5 );
	var moverPro1_4 = new Dancer( "moverPro1_4", "PaperDancerForMoves", [0, 0.6,  0], [3.14,-1.50, 0], scaleDancers, copy(moversBoardPro), "showFormURL", 0.01, "outsideTime", 4.5 );
	var moversPro1 = new Sequence ( "moversPro1", [moverPro1_1, moverPro1_2, moverPro1_3, moverPro1_4], "");


/*
	var proximityMan = new Dancer( "proximityMan", "PaperDancer", [0, 0.45, 0], [0, 0, 0], scaleDancers, copy(animBoard),"", 0, "proximity" );
	var proximityManScene = new Sequence ( "proximityMan", [proximityMan]);

	var multipleDancer1 = new Dancer( "arrowAnim14", "PaperDancer", [0, 0.6, 0], [0, 0, 0], [0.1, 0.1, 0.1], copy(animBoard),"", 0.1, "active" );
	var multipleDancer2 = new Dancer( "arrowAnim15", "PaperDancer", [0.1, 0.7, 0], [0, 0, 0], [0.1, 0.1, 0.1], copy(animBoard),"", 0.1, "active" );
	var multipleDancer3 = new Dancer( "arrowAnim16", "PaperDancer", [0.2, 0.8, 0], [0, 0, 0], [0.1, 0.1, 0.1], copy(animBoard),"", 0.1, "active" );
	var multipleDancer4 = new Dancer( "arrowAnim17", "PaperDancer", [0.3, 0.9, 0], [0, 0, 0], [0.1, 0.1, 0.1], copy(animBoard),"moversRotation", 0.1, "active" );
	var multipleTouchSequence = new Sequence ( "multipleTouch", [multipleDancer1, multipleDancer2, multipleDancer3,multipleDancer4], "multiple" );
*/
	

	
	var allObjectsScene1Array = [squareTown, telescopStructure];

	var scene1 = new Scene ( allObjectsScene1Array, [introScene, coasterMan, fromRoofDancers1, fromRoofDancers2, eightDancers1, eightDancers2, fromSidesDancers1,fromSidesDancers2, plenty, moversRot1, moversRot2, moversPro1] );

	var counterManager = new CounterManager (counter);

	var gameManager = new GameManager ( [scene1] );

	gameManager.start();

	onRenderFcts.push(function(delta){
		gameManager.update(delta);
	})

	//////////////////////////////////////////////////////////////////////////////////
	//		render the whole thing on the page
	//////////////////////////////////////////////////////////////////////////////////

	// render the scene
	onRenderFcts.push(function(){
		renderer.render( scene, camera );
	})

	// run the rendering loop
	var lastTimeMsec= null
	requestAnimationFrame(function animate(nowMsec){
		// keep looping
		requestAnimationFrame( animate );
		// measure time
		lastTimeMsec	= lastTimeMsec || nowMsec-1000/60
		var deltaMsec	= Math.min(200, nowMsec - lastTimeMsec)
		lastTimeMsec	= nowMsec
		// call each update function
		onRenderFcts.forEach(function(onRenderFct){
			onRenderFct(deltaMsec/1000, nowMsec/1000)
		})
	})


});
